import unittest
import query
from query import Query
import edison
from edison import *
import json


class Test(unittest.TestCase):
    def test_clean_sentences(self):
        """
			Testing the clean question function if it replace some specific 
			words like: "is", "are",... from the question asked by user using KB
		"""
        input_question = "who was is frodo"
        expected_output = "who frodo"
        query = Query()
        actual_output = query.clean_sentences(input_question)
        self.assertEqual(actual_output, expected_output)

    def test_match_question_matches(self):
        """
			Testing the match_question function for a question that exists in the 
			knowledge base. It should find the closest match of the user question from the knowledge base and return its id
			from the knowlwdge	   
		"""
        user_question = "where did bilbo go to live after shire"
        expected_kb_question_id = 11
        query = Query()
        actual_question_id = query.match_question(user_question)
        self.assertEqual(expected_kb_question_id, actual_question_id)

    def test_match_question_no_match(self):
        """
			Testing the match_question function for a question whose match does not 
			exist in the knowledge base. When no matching question is found it should return -2
		"""
        user_question = "who killed frodo"
        expected_kb_question_id = -2
        query = Query()
        kb_question_id = query.match_question(user_question)
        self.assertEqual(kb_question_id, expected_kb_question_id)

    def test_get_answer(self):
        """
			To test if given a question id, the get_answer function can return the answer of
			the with id same as given id   
		"""
        question_id = 2
        expected_output = "Hobbits, also known as Halflings, were an ancient mortal race that lived in Middle-earth."
        query = Query()
        query.setup_questions()
        actual_output = query.questions[question_id]["answer"]
        self.assertEqual(actual_output, expected_output)

    def test_get_question(self):
        """
			To test if given a question id, the get_question function can return the question
			with id same as given id   
		"""
        question_id = 2
        expected_output = "who are the Hobbits"
        query = Query()
        query.setup_questions()
        actual_output = query.questions[question_id]["question"]
        self.assertEqual(actual_output, expected_output)

    def test_search_kibana(self):
        """
		    To test the search_kibana function. Given an inner query(json format), whether we can get response
		    from Edison backend(kibana)
		"""
        inner_query = """[{ "match": { "text": "Baggins" } }, { "match": { "text": "birthday" } }, { "match": { "text": "celebrating" } }]"""
        edison_response = search_kibana(inner_query)
        output = "When Mr. Bilbo Baggins of Bag End announced that hewould shortly be celebrating his eleventy-ﬁrst birthday witha party of special magniﬁcence, there was much talk andexcitement in Hobbiton."
        self.assertEqual(output, edison_response["hits"]["hits"][0]["_source"]["text"])

    def test_get_response_kibana(self):
        """
			Given a user query, if we can get response from Edison backend(kibana) using get_response_kibana function
		"""
        question = "which birthday was Bilbo Baggins celebrating ?"
        response_edison = get_response_kibana(question)
        output = "When Mr. Bilbo Baggins of Bag End announced that hewould shortly be celebrating his eleventy-ﬁrst birthday witha party of special magniﬁcence, there was much talk andexcitement in Hobbiton."
        self.assertEqual(output, response_edison)

    def test_return_filtered_sentence(self):
        """
			Testing the return_filtered_sentence function to convert a query to filtered sentence, 
			i.e. remove extra words added to stop words list and return a list.
		"""
        question = "Who made the One Ring ?"
        expected_output = ["made", "One", "Ring"]
        actual_output = return_filtered_sentence(question)
        self.assertEqual(expected_output, actual_output)

    def test_get_inner_query(self):
        """
			Testing the get_inner_query function to crerate a user query into inner json query
		"""
        question = "Who made the One Ring?"
        expected_output = """[{"match": {"text": "made"}}, {"match": {"text": "One"}}, {"match": {"text": "Ring"}}]"""
        filtered_question = return_filtered_sentence(question)
        actual_output = get_inner_query(filtered_question)
        self.assertEqual(expected_output, actual_output)


if __name__ == "__main__":
    unittest.main()
